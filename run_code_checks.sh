#!/usr/bin/env bash

function wait_for_schema_registry() {

    if [[ -z ${SCHEMA_REGISTRY+x} ]]; then SCHEMA_REGISTRY="http://localhost:8081"; fi

    line=`echo ${SCHEMA_REGISTRY} | tr -s "/:" " "`
    read -a arr <<< ${line}
    SCHEMA_REGISTRY_HOST=${arr[1]}
    SCHEMA_REGISTRY_PORT=${arr[2]}

    echo schema registry host: ${SCHEMA_REGISTRY_HOST}
    echo schema registry port: ${SCHEMA_REGISTRY_PORT}

    echo -e "\n"
    RETRIES=1
    while : ; do
      echo "Waiting for kafka to get ready $RETRIES"
      sleep 1
      python cub.py sr-ready ${SCHEMA_REGISTRY_HOST} ${SCHEMA_REGISTRY_PORT} 1 #> /dev/null 2>&1
      if [[ "$?" -eq "0" ]]; then
          break
      fi
      let "RETRIES++"
    done
}

wait_for_schema_registry

set -e

cd avro_preprocessor

#echo "Running pylint (errors-only)"
#find "$(pwd -P)" -iname "*.py" | xargs -L 1 pylint --errors-only

#echo "Running pylint (exit-zero)"
#find "$(pwd -P)" -iname "*.py" | xargs -L 1 pylint --exit-zero

echo "Running mypy"
python3 -m pip install types-requests
MYPYPATH=. find "$(pwd -P)" -iname "*.py" | xargs mypy --strict --allow-untyped-decorators --allow-any-generics --check-untyped-defs --ignore-missing-imports

echo "Running unit tests"
cd ..
find "$(pwd -P)/avro_preprocessor/" -iname "*.py" | xargs -L 1 python3 -m unittest
